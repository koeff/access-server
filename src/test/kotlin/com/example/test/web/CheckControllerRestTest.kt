package com.example.test.web

import com.example.test.service.AccessService
import io.mockk.coEvery
import io.mockk.mockk
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.reactive.server.WebTestClient

@WebFluxTest(controllers = [CheckControllerRest::class])
@ContextConfiguration(classes = [CheckControllerRestTest.TestConfiguration::class])
class CheckControllerRestTest {

    @Autowired
    private lateinit var webClient: WebTestClient

    @Autowired
    private lateinit var accessService: AccessService

    @Test
    fun `check granted`() {
        coEvery {
            accessService.check(any(), any(), any())
        } returns AccessService.Response.Granted

        webClient.get().uri {
            it.path("/check")
                .queryParam("keyId", 1)
                .queryParam("entrance", true)
                .queryParam("roomId", 1)
                .build()
        }
            .exchange()
            .expectStatus().isOk
    }

    @Test
    fun `check denied`() {
        coEvery {
            accessService.check(any(), any(), any())
        } returns AccessService.Response.Denied("")

        webClient.get().uri {
            it.path("/check")
                .queryParam("keyId", 1)
                .queryParam("entrance", true)
                .queryParam("roomId", 1)
                .build()
        }
            .exchange()
            .expectStatus().isForbidden
    }

    @Test
    fun `incomplete param list`() {
        coEvery {
            accessService.check(any(), any(), any())
        } returns AccessService.Response.Denied("")

        webClient.get().uri {
            it.path("/check")
                .queryParam("keyId", 1)
                .build()
        }
            .exchange()
            .expectStatus().isBadRequest
    }

    @Test
    fun `internal server error`() {
        coEvery {
            accessService.check(any(), any(), any())
        } throws NullPointerException("ooops")

        webClient.get().uri {
            it.path("/check")
                .queryParam("keyId", 1)
                .queryParam("entrance", true)
                .queryParam("roomId", 1)
                .build()
        }
            .exchange()
            .expectStatus().is5xxServerError
    }

    @Configuration
    class TestConfiguration {
        @Bean
        fun accessService() = mockk<AccessService>()

        @Bean
        fun checkController(accessService: AccessService) =
            CheckControllerRest(accessService)
    }
}