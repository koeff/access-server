package com.example.test.service

import io.kotlintest.matchers.beInstanceOf
import io.kotlintest.should
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import kotlin.reflect.KClass

class AccessServiceImplTest {

    private val accessService: AccessService = AccessServiceImpl()

    @ParameterizedTest
    @MethodSource("testArgs")
    fun `test access`(
        keyId: Int, entrance: Boolean, roomId: Int,
        klass: KClass<AccessService.Response>
    ) = runBlocking {
        val result = accessService.check(keyId, entrance, roomId)

        result should beInstanceOf(klass)
    }

    companion object {
        @JvmStatic
        fun testArgs(): List<Arguments> = listOf(
            Arguments.of(1, true, 1, AccessService.Response.Granted::class),
            Arguments.of(1, true, 2, AccessService.Response.Granted::class),
            Arguments.of(1, true, 3, AccessService.Response.Granted::class),
            Arguments.of(1, true, 4, AccessService.Response.Granted::class),
            Arguments.of(1, true, 5, AccessService.Response.Granted::class),

            Arguments.of(2, true, 1, AccessService.Response.Denied::class),
            Arguments.of(2, true, 2, AccessService.Response.Granted::class),
            Arguments.of(2, true, 3, AccessService.Response.Denied::class),
            Arguments.of(2, true, 4, AccessService.Response.Granted::class),
            Arguments.of(2, true, 5, AccessService.Response.Denied::class)
        )
    }
}