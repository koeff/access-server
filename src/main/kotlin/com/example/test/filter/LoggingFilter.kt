package com.example.test.filter

import org.apache.logging.log4j.kotlin.Logging
import org.springframework.web.server.ServerWebExchange
import org.springframework.web.server.WebFilter
import org.springframework.web.server.WebFilterChain
import reactor.core.publisher.Mono

class LoggingFilter : Logging, WebFilter {
    override fun filter(exchange: ServerWebExchange, chain: WebFilterChain): Mono<Void> {
        val request = exchange.request
        logger.info {
            "REQ:  ${request.method}" +
                " path=${request.path.pathWithinApplication()}" +
                " params=[${request.queryParams}]" +
                " id=[${request.id}]"
        }

        return chain.filter(exchange)
            .doFinally {
                logger.info { "RESP: ${exchange.response.statusCode} id=[${request.id}]" }
            }
    }
}