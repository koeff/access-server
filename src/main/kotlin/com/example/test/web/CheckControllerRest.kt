package com.example.test.web

import com.example.test.service.AccessService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class CheckControllerRest(
    private val accessService: AccessService
) {

    @GetMapping("/check")
    suspend fun check(
        @RequestParam roomId: Int,
        @RequestParam entrance: Boolean,
        @RequestParam keyId: Int
    ): ResponseEntity<Unit> {
        val status = try {
            when (accessService.check(roomId, entrance, keyId)) {
                AccessService.Response.Granted ->
                    HttpStatus.OK

                is AccessService.Response.Denied ->
                    HttpStatus.FORBIDDEN
            }
        } catch (ex: Exception) {
            HttpStatus.INTERNAL_SERVER_ERROR
        }

        return ResponseEntity
            .status(status)
            .build()
    }
}