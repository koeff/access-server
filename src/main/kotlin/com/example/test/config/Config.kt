package com.example.test.config

import com.example.test.filter.LoggingFilter
import com.example.test.service.AccessService
import com.example.test.service.AccessServiceImpl
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.server.WebFilter

@Configuration
class Config {

    @Bean
    fun accessService(): AccessService = AccessServiceImpl()

    @Bean
    fun loggingFilter(): WebFilter = LoggingFilter()

}