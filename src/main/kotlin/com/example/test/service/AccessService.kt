package com.example.test.service

interface AccessService {

    suspend fun check(roomId: Int, entrance: Boolean, keyId: Int): Response

    sealed class Response {

        object Granted : Response()

        data class Denied(
            val details: String
        ) : Response()
    }

}