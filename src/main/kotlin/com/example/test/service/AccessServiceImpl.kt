package com.example.test.service

import org.apache.logging.log4j.kotlin.Logging
import java.util.concurrent.ConcurrentHashMap

class AccessServiceImpl : AccessService {

    companion object : Logging {
        private const val INITIAL_ROOM_ID = 0
    }

    // current key location, should be persisted somewhere (any DB, IMDG...)
    private val keyLoc: MutableMap<Int, Int> = ConcurrentHashMap()

    override suspend fun check(
        roomId: Int,
        entrance: Boolean,
        keyId: Int
    ): AccessService.Response {
        var result: AccessService.Response = AccessService.Response.Granted
        keyLoc.compute(keyId) { _, room ->
            val curRoomId = room ?: INITIAL_ROOM_ID
            if (entrance) {
                if (curRoomId == INITIAL_ROOM_ID && canEnter(keyId, roomId)) {
                    // grant
                    roomId
                } else {
                    // deny
                    result = AccessService.Response.Denied("access denied or already in room")
                    curRoomId
                }
            } else {
                if (curRoomId == roomId) {
                    // grant
                    INITIAL_ROOM_ID
                } else {
                    // deny
                    logger.error("user with key: $keyId tried exit from room $roomId, but current room is $curRoomId")
                    result = AccessService.Response.Denied("access denied or already in room")
                    curRoomId
                }
            }
        }

        return result
    }

    private fun canEnter(keyId: Int, roomId: Int): Boolean {
        return keyId % roomId == 0
    }

}